#!/bin/bash
source ../Secrets/fs.creds
echo "***Backing up FS switches***"
ansible-playbook fs.backup.yml -vv
